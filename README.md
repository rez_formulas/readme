**Table of Contents**

[TOC]

# Overview
![rez_formulas Icon](https://bitbucket.org/account/rez_formulas/avatar
"rez_formulas Avatar")

`rez_formulas` is a collection of [`rez`](https://github.com/nerdvegas/rez
"rez") packages for common, non-private software packages, commonly referred to
as `rez installers` or `rezipes`.

[`rez`](https://github.com/nerdvegas/rez "rez") is "cross-platform package
manager", and you can learn more about it on its [project page](https://github.com/nerdvegas/rez
"rez").

# Installer Philosophies
## Categorisation/Categorization
For organisation/organization, packages are grouped into Bitbucket projects,
just to show similarity in approach.  The projects are as follows (in
conceptual order):


| Project                                                 | Project                        | Price |
| --------------------------------------------------------|--------------------------------|-------|
| ![Core Project Avatar][REZ_CORE]                        | [Core]                         | `rez` core packages of `platform`, `arch`, and `os` |
| ![Commercial Proprietary Project Avatar][REZ_CP]        | [Commercial Proprietary]       | Non-private and non-open-source |
| ![Open-Source Software Project Avatar][REZ_OSS]         | [Open-Source Software]         | Open-source, but does not require rebuilding ([REZ_CPP](https://bitbucket.org/account/user/rez_formulas/projects/REZ_CCR "REZ_CCR")) or is from the Python Package Index ([REZ_PYPI](https://bitbucket.org/account/user/rez_formulas/projects/REZ_PYPI "REZ_CPP")) |
| ![Commercial Compiled Rebuilds Project Avatar][REZ_CCR] | [Commercial Compiled Rebuilds] | Non-private (i.e., commercial) and is rebuilt on `rez-release` |
| ![Python Package Index (PyPI) Project Avatar][REZ_PYPI] | [Python Package Index (PyPI)]  | From the [Python Package Index (PyPI)](https://pypi.org/ "PyPI") |
| ![Commercial Python Project Avatar][REZ_CPP]            | [Commercial Python]            | Not from the Python Package Index ([REZ_PYPI](https://bitbucket.org/account/user/rez_formulas/projects/REZ_PYPI "REZ_CPP")) (This project may be merged into the others in the future) |
[REZ_CORE]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CORE/avatar/8  "Core Project Avatar"
[REZ_CP]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CP/avatar "Commercial Proprietary Project Avatar"
[REZ_OSS]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_OSS/avatar "Open-Source Software Project Avatar"
[REZ_CCR]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CCR/avatar "Commercial Compiled Rebuilds Project Avatar"
[REZ_PYPI]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_PYPI/avatar "PyPI Project Avatar"
[REZ_CPP]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CPP/avatar "Commercial Python Project Avatar"
[Core]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CORE "REZ_CORE"
[Commercial Proprietary]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CP "REZ_CP"
[Open-Source Software]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_OSS "REZ_OSS"
[Commercial Compiled Rebuilds]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CCR "REZ_CCR"
[Python Package Index (PyPI)]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_PYPI "REZ_PYPI"
[Commercial Python]: https://bitbucket.org/account/user/rez_formulas/projects/REZ_CPP "REZ_CPP"

Loose definitions of what constitutes "commercial" (i.e., non-private) and
"proprietary" (i.e., non-open-source) are based on the [GNU software
categories](https://www.gnu.org/philosophy/categories.html
"Categories of Free and Non-Free Software")


## Target OS
Installs are usually initially targeted towards an OS of `linux`, architecture
of `x86_64`, and platform of `CentOS-7`.

However, the point of this project is to head towards more complete and robust
installers for a variety of combinations.


## Naming
In a production environment, we believe `git` repository names should match
package names (in the package definition/project files below).

We also believe that package names should be as intuitive as possible for `rez`
command-line patching.

Therefore, we generally follow the logic below for naming, however, sometimes
we adapt in order to be more intuitive:

* Names should adhere as closely to the software name as possible, however, to
simplify command-line patching, we generally use only lowercase letters
    * Example: `awesomebump` for "[AwesomeBump](https://github.com/kmkolasinski/AwesomeBump
    "AwesomeBump")"
* Since `rez`'s standard delimiter for versions is a [hyphen-minus](https://en.wikipedia.org/wiki/Hyphen-minus
"hyphen-minus"),
we substitute underscores (`_`) in names, or omit it altogether
    * Example: `vray` for "V-Ray"
* We do not put vendor or author names in the package name
    * Example: No `foundry_nuke` or `autodesk_maya`
* Multi-word names use underscores (`_`) for spaces, with some words omitted
    * Example: `vray_maya` for "V-Ray for Maya"


## Versioning
Usually vendors are able to clearly identify version numbers.  But sometimes
there are certain cases where this doesn't work or isn't clear.

Any non-standard versioning logic will be identified in the package definition/
project file (see below).


## Source Files
Since many user environments no longer have internet access, we try to always
provide a way to `rez-release` from a downloaded file, with instructions to put
in the `src/` directory.

Some packages are setup as well to download directly from source if possible.

Note: In order for `git` to maintain an "empty" directory in its repo, we place
a `.dummy` file inside the `src/` directory.


## Payloads
Packages are designed to install an entire software's contents into the package
itself--no symlinks or separate payload location.  This helps to keep the
package consolidated, and although any additional releases will results in
larger package sizes, we hope that once you adapt the installer for your own
needs, additional releases of the same version will be rare.

## Standard `.gitignore`
Since `rez` uses `git` by default to tag commits for specific releases, and
releases always create `build/` directories, a `.gitignore` file will be in
every installer with a minimum of ignoring `build/`.

## Package Definition/Project Files
Every `rez` package has a "definition" or "project" file that instructs `rez`
on how to treat it.  We use the Python version of the file (`package.py`), and
have some specific philosophies that we follow:

### Order
All our packages have a custom order of attributes.  Since upon release, `rez`
will re-order the attributes in the released version of the file, we choose our
own order that makes logical sense for authoring.

Attributes listed below are "Standard" (meaning native to `rez`) or "Arbitrary"
(meaning, we have created it ourselves).  We also indicate if it's required for
every package by `rez` or by us.  General logic behind the order is:

* Required attributes
* Attributes more often to be changed or updated
* Conceptual order of what people look for when browsing

Note: This is not a description of every attribute, and we only make comments
as relevant to our packages--attribute descriptions can be found in the
[rez wiki](https://github.com/nerdvegas/rez/wiki/Package-Definition-Guide#standard-package-attributes
"rez Package Definition Guide")).

|    | Attribute                | Type      | Requirement    | Description |
|----|--------------------------|-----------|----------------|-------------|
|  1 | `name`                   | Standard  | `rez` required | Obviously the most important identifier to the package (at least, at a human-level) |
|  2 | `version`                | Standard  | `rez` required | Most commonly changed attribute (once an installer is adopted for your environment, it is usually the ***only*** attribute you change)--essentially required (you can indeed have versionless-packages, but you would get yourself in trouble very quickly) |
|  3 | `uuid`                   | Standard  | `rez` strongly suggested | |
|  4 | `description`            | Standard  | Required by us | |
|  5 | `authors`                | Standard  | Required by us | |
|  6 | `help`                   | Standard  | If exists      |
|  7 | `package_type`           | Arbitrary | Required by us | Our packages all have this custom attribute.  Value is "external" for all our packages, but other values can be used as you need.
|  8 | `with scope(''):`        | Standard  | As needed      | Any config-based values--we don't have any because we don't need to, but this is where we suggest these go if you need to add them
|  9 | `license_category`       | Arbitrary | Required by us | Values can be `open-source` or whatever you need.
| 10 | `license_type`           | Arbitrary | If exists      | We have found that people would like to query by specific licensing requirements, so we have added this.  Values are suggested to match the [GitHub license types](https://help.github.com/en/articles/licensing-a-repository#searching-github-by-license-type "GitHub license types")
| 11 | `license_help`           | Arbitrary | If exists      | Link to software's license page
| 12 | `has_plugins`            | Standard  | Only if `True` |
| 13 | `plugin_for`             | Standard  |
| 14 | `requires`               | Standard  |
| 15 | `build_requires`         | Standard  |
| 16 | `private_build_requires` | Standard  |
| 17 | `variants`               | Standard  |
| 18 | `tools`                  | Standard  |
| 19 | `preprocess`             | Standard  |
| 20 | `pre_commands`           | Standard  |
| 21 | `commands`               | Standard  |
| 22 | `post_commands`          | Standard  |
| 23 | `tests`                  | Standard  |
| 24 | `build_command`          | Standard  |

`TODO`: Clarify location of `build_command` and `preprocess`
`TODO`: Consider adding `tags`

### Syntax
As we use the Python version of the file, we generally follow the [Google
Python Style Guide](https://google.github.io/styleguide/pyguide.html "Google Python Style Guide")
with some specific decisions:

* Indentation: for things like multi-line strings, lists, and other types, we
open on the first line, indent by 4 spaces, and close on its own line, matching
indentation of the first line
    * Example:

            list = [
                'item',
            ]

* Single-quotes for strings
* [Trailing commas in sequences of items](https://google.github.io/styleguide/pyguide.html#341-trailing-commas-in-sequences-of-items "Google Python Style Guide")

### UUIDs
`rez` requires a UUID per package family to prevent packages with matching names
from accidentally being released.

We don't want to presume that our UUIDs are global, so we have set all to
`00000000-0000-0000-0000-000000000000`.

`rez` treats the `uuid` attribute just as a string, so it could have just been
`00000000000000000000000000000000` (or `foo`, for that matter), however, we
wanted it to still be identified as a UUID, so we have maintained the standard
UUID format.

### Package-Specific Environment Variables
We often include a large number of environment variables in the `commands`
section, just to give the user clear understanding of commonly-set variables.
We leave many commented-out, or others with dummy info, because it would have
to be adopted to your environment anyway.